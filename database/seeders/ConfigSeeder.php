<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('configs')->insert([
            'logo' => '',
            'favicon' => '',
            'title' => 'Laravel-Demo',
            'mobile_no' => '1234567890',
            'email' => 'admin@example.com',
            'address' => 'Company Address',
            'footer' => 'All rights Reserved by Laravel-Demo',
        ]);
    }
}
