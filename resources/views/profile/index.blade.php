@include('layouts.partials.header')
@include('layouts.partials.sidebar')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
	<div class="d-flex flex-column flex-column-fluid">
		<div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10">
			<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
				<div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
					<div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
						<h1 class="page-heading d-flex flex-column justify-content-center text-dark fw-bold fs-3 m-0">Account Settings</h1>
						<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
							<li class="breadcrumb-item text-muted">
								<a href="{{ route('home') }}" class="text-muted text-hover-primary">Home</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">Account</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="kt_app_content" class="app-content flex-column-fluid">
			<div id="kt_app_content_container" class="app-container container-fluid">
				@if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{!! \Session::get('success_message') !!}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (\Session::has('error'))
				    <div class="alert alert-danger alert-dismissible fade show" role="alert">
				      <strong>{!! \Session::get('error') !!}</strong>
				      <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
				    </div>
				@endif
				<div class="card mb-5 mb-xl-10">
					<div class="card-body pt-9 pb-0">
						<div class="d-flex flex-wrap flex-sm-nowrap">
							<div class="me-7 mb-4">
								<div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
									 @if(!is_null(auth()->user()->photo))
                                    <img src="{{ asset('Profile/photos/' . auth()->user()->photo) }}" alt="user" />
                                    @else
                                    <img src="{{ asset('admin/dist/assets/media/svg/avatars/blank-dark.svg') }}" alt="user" />
                                    @endif
								</div>
							</div>
							<div class="flex-grow-1">
								<div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
									<div class="d-flex flex-column">
										<div class="d-flex align-items-center mb-2">
											<span class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">{{ $user['name'] }}</span>
											
										</div>
										<div class="d-flex flex-wrap fw-semibold fs-6 mb-4 pe-2">
											<span class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
											<i class="ki-outline ki-sms fs-4 me-1"></i>{{ $user['email'] }}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-5 mb-xl-10">
					<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
						<div class="card-title m-0">
							<h3 class="fw-bold m-0">Profile Details</h3>
						</div>
					</div>
					<div id="kt_account_settings_profile_details" class="collapse show">
						<form action="{{ route('profile.update', $user['id']) }}" method="POST" class="form" enctype="multipart/form-data">
							@csrf
                  			@method('PUT')
							<div class="card-body border-top p-9">
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Profile Image</label>
									<div class="col-lg-8">
										<div class="image-input image-input-outline" data-kt-image-input="true">
											<div class="image-input-wrapper w-125px h-125px" style="background-image: {{ isset($user) && auth()->user()->photo ? 'url('.asset('Profile/photos/'. auth()->user()->photo).')' : 'none' }};"></div>
											<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
												<i class="ki-outline ki-pencil fs-7"></i>
												<input type="file" name="photo" accept=".png, .jpg, .jpeg" />
												<input type="hidden" name="custom_delete_photo" />
											</label>
											<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel photo">
												<i class="ki-outline ki-cross fs-2"></i>
											</span>
											<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
												<i class="ki-outline ki-cross fs-2"></i>
											</span>
										</div>
										<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label required fw-semibold fs-6">Name & Email</label>
									<div class="col-lg-8">
										<div class="row">
											<!--begin::Col-->
											<div class="col-lg-6 fv-row">
												<input type="text" name="name" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0 @error('name') is-invalid @enderror" placeholder="Mandal name" value="{{ old('name', optional($user)->name) }}" />
												@error('name')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
											<div class="col-lg-6 fv-row">
												<input type="email" name="email" class="form-control form-control-lg form-control-solid @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', optional($user)->email) }}" />
												@error('name')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Street Address</label>
									<div class="col-lg-8 fv-row">
										<input type="text" name="street_address" class="form-control form-control-lg form-control-solid" placeholder="Street address" value="{{ old('street_address', optional($user)->street_address) }}" />
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label required fw-semibold fs-6">City & Taluka</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-6 fv-row">
												<input type="text" name="city" class="form-control form-control-lg form-control-solid @error('city') is-invalid @enderror" placeholder="City" value="{{ old('city', optional($user)->city) }}" />
												@error('city')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
											<div class="col-lg-6 fv-row">
												<input type="text" name="taluka" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0 @error('taluka') is-invalid @enderror" placeholder="Taluka" value="{{ old('taluka', optional($user)->taluka) }}" />
												@error('taluka')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">District & State</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-6 fv-row">
												<input type="text" name="district" class="form-control form-control-lg form-control-solid @error('district') is-invalid @enderror" placeholder="District" value="{{ old('district', optional($user)->district) }}" />
												@error('district')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
											<div class="col-lg-6 fv-row">
												<input type="text" name="state" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0 @error('state') is-invalid @enderror" placeholder="State" value="{{ old('state', optional($user)->state) }}" />
												@error('state')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Pin No</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-6 fv-row">
												<input type="text" name="pin_code" class="form-control form-control-lg form-control-solid @error('pin_code') is-invalid @enderror" placeholder="Pin code" value="{{ old('pin_code', optional($user)->pin_code) }}" />
												@error('pin_code')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer d-flex justify-content-end py-6 px-9">
								<button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2">Discard</button>
								<button type="submit" value="Update" class="btn btn-sm btn-primary">Save Changes</button>
							</div>
						</form>
					</div>
				</div>
				<div class="card mb-5 mb-xl-10">
					<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_signin_method">
						<div class="card-title m-0">
							<h3 class="fw-bold m-0">Sign-in Method</h3>
						</div>
					</div>
					<div id="kt_account_settings_signin_method" class="collapse show">
						<div class="card-body border-top p-9">
							<div class="d-flex flex-wrap align-items-center">
								<div id="kt_signin_email">
									<div class="fs-6 fw-bold mb-1">Password</div>
									<div class="fw-semibold text-gray-600">************</div>
								</div>
								<div id="kt_signin_email_edit" class="flex-row-fluid d-none">
									<form method="POST" class="form" action="{{ route('profile.change', $user['id']) }}">
										@csrf
										<div class="row mb-6">
											<div class="col-lg-6 mb-4 mb-lg-0">
												<div class="fv-row mb-0">
													<label for="password" class="form-label fs-6 fw-bold mb-3">Enter New Password</label>
													<input type="password" class="form-control form-control-lg form-control-solid @error('password') is-invalid @enderror" id="password" placeholder="New Password" name="password" value="" />
													@error('password')
		                                                <span class="invalid-feedback" role="alert">
		                                                    <strong>{{ $message }}</strong>
		                                                </span>
		                                            @enderror
												</div>
											</div>
											<div class="col-lg-6">
												<div class="fv-row mb-0">
													<label for="password" class="form-label fs-6 fw-bold mb-3">Confirm Password</label>
													<input type="password" class="form-control form-control-lg form-control-solid" name="confirm-password" id="password" />
													@error('password')
		                                                <span class="invalid-feedback" role="alert">
		                                                    <strong>{{ $message }}</strong>
		                                                </span>
		                                            @enderror
												</div>
											</div>
										</div>
										<div class="d-flex">
											<button type="submit" value="Add" class="btn btn-sm btn-primary">
												<span class="indicator-label">Save Changes</span>
											</button>
											<button id="kt_signin_cancel" type="button" class="btn btn-sm btn-color-gray-400 btn-active-light-primary px-6">Cancel</button>
										</div>
									</form>
								</div>
								<div id="kt_signin_email_button" class="ms-auto">
									<button class="btn btn-light btn-active-light-primary">Change Password</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.partials.footer')

<script src="{{ asset('admin/dist/assets/js/custom/account/settings/signin-methods.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/account/settings/profile-details.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/account/settings/deactivate-account.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/pages/user-profile/general.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-campaign.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/type.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/details.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/finance.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/complete.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/main.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/two-factor-authentication.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>
