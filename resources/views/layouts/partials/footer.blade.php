                <div id="kt_app_footer" class="app-footer">
                    <div class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted fw-semibold me-1">{{ now()->year }}&copy;{{$config->footer}} </span>Powered By
                            <a href="https://webcreta.com" target="_blank" class="text-gray-800 fs-6 text-hover-primary">Webcreta</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <script>var hostUrl = "assets/";</script>
        <script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/scripts.bundle.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
        <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/map.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/worldLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/continentsLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/usaLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZonesLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZoneAreasLow.js"></script>
        <script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>
    </body>
</html>