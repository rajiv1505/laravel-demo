<!DOCTYPE html>
<html lang="en">
    <head><base href=""/>
        @if(isset($config->title))
        <title>{{$config->title}}</title>
        @else
        <title>Laravel-Demo</title>
        @endif
        <meta charset="utf-8" />
        <meta name="description" content="Laravel-Demo" />
        <meta name="keywords" content="Laravel-Demo" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Laravel-Demo" />
        <meta property="og:url" content="Laravel-Demo" />
        <meta property="og:site_name" content="Laravel-Demo" />
        <link rel="canonical" href="#" />
        @if(isset($config->favicon))
        <link rel="shortcut icon" href="{{asset('Config/Favicon/').'/'.$config->favicon}}" />
        @else
        <link rel="shortcut icon" href="{{ asset('admin/dist/assets/media/misc/lara-fav.png') }}" />
        @endif
        <!--begin::Fonts(mandatory for all pages)-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
        <!--end::Fonts-->
        <!--begin::Vendor Stylesheets(used for this page only)-->
        <link href="{{ asset('admin/dist/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Vendor Stylesheets-->
        <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
        <link href="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/dist/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    </head>

    <body id="kt_app_body" data-kt-app-header-fixed="true" data-kt-app-header-fixed-mobile="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" class="app-default">
        <script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if ( localStorage.getItem("data-bs-theme") !== null ) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>

        <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
            <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
                <div id="kt_app_header" class="app-header">
                    <div class="app-container container-fluid d-flex align-items-stretch flex-stack" id="kt_app_header_container">
                        <div class="d-flex align-items-center d-block d-lg-none ms-n3" title="Show sidebar menu">
                            <div class="btn btn-icon btn-active-color-primary w-35px h-35px me-2" id="kt_app_sidebar_mobile_toggle">
                                <i class="ki-outline ki-abstract-14 fs-2"></i>
                            </div>
                            <a href="{{ route('home')}}">
                                <img alt="Logo" src="{{ asset('admin/dist/assets/media/misc/baps_logo.svg') }}" class="h-30px" />
                            </a>
                        </div>
                        <div class="app-navbar flex-lg-grow-1" id="kt_app_header_navbar">
                            <div class="app-navbar-item d-flex align-items-stretch flex-lg-grow-1">
                                <div id="kt_header_search" class="header-search d-flex align-items-center w-lg-200px" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-search-responsive="true" data-kt-menu-trigger="auto" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-start">
                                     
                                </div>
                            </div>
                            <div class="app-navbar-item ms-1 ms-md-3" id="kt_header_user_menu_toggle">
                                <div class="cursor-pointer symbol symbol-circle symbol-35px symbol-md-45px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                    @if(!is_null(auth()->user()->photo))
                                    <img src="{{ asset('Profile/photos/' . auth()->user()->photo) }}" alt="user" />
                                    @else
                                    <img src="{{ asset('admin/dist/assets/media/svg/avatars/blank-dark.svg') }}" alt="user" />
                                    @endif
                                </div>
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
                                    <div class="menu-item px-3">
                                        <div class="menu-content d-flex align-items-center px-3">
                                            @if(!is_null(auth()->user()->photo))
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="{{ asset('Profile/photos/' . auth()->user()->photo) }}" />
                                                </div>
                                            @else
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="{{ asset('admin/dist/assets/media/svg/avatars/blank-dark.svg') }}" />
                                                </div>
                                            @endif
                                            <div class="d-flex flex-column">
                                                <div class="fw-bold d-flex align-items-center fs-5">{{ Auth::user()->name }}
                                                <span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2"></span></div>
                                                <a href="#" class="fw-semibold text-muted text-hover-primary fs-7">{{ Auth::user()->email }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="separator my-2"></div>
                                    <div class="menu-item px-5">
                                        <a href="{{ route('profile.index') }}" class="menu-link px-5">My Profile</a>
                                    </div>
                                    <div class="separator my-2"></div>
                                    <div class="menu-item px-5 my-1">
                                        <a href="{{ route('config.index') }}" class="menu-link px-5">Setting</a>
                                    </div>
                                    <div class="menu-item px-5">
                                        <a class="menu-link px-5" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign Out
                                        </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="app-navbar-separator separator d-none d-lg-flex"></div>
                    </div>
                </div>