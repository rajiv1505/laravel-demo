<div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
    <div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
        <div class="app-sidebar-header d-flex flex-stack d-none d-lg-flex pt-8 pb-2" id="kt_app_sidebar_header">
            <a href="{{ route('home')}}" class="app-sidebar-logo">
                <img alt="Logo" src="{{ asset('admin/dist/assets/media/misc/laravel.png') }}" class="h-25px d-none d-sm-inline app-sidebar-logo-default theme-light-show" />
                <img alt="Logo" src="{{ asset('admin/dist/assets/media/misc/laravel.png') }}" class="h-20px h-lg-25px theme-dark-show" />
            </a>
            <div id="kt_app_sidebar_toggle" class="app-sidebar-toggle btn btn-sm btn-icon bg-light btn-color-gray-700 btn-active-color-primary d-none d-lg-flex rotate" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="app-sidebar-minimize">
                <i class="ki-outline ki-text-align-right rotate-180 fs-1"></i>
            </div>
        </div>
        <div class="app-sidebar-navs flex-column-fluid py-6" id="kt_app_sidebar_navs">
            <div id="kt_app_sidebar_navs_wrappers" class="app-sidebar-wrapper hover-scroll-y my-2" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_header" data-kt-scroll-wrappers="#kt_app_sidebar_navs" data-kt-scroll-offset="5px">
                <div class="app-sidebar-menu-secondary menu menu-rounded menu-column mb-6">
                    <div class="menu-item mb-2">
                        <div class="menu-heading text-uppercase fs-7 fw-bold">
                            Menu
                        </div>
                        <div class="app-sidebar-separator separator"></div>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*home*') ? 'active' : ''}}" href="{{ route('home') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-home-2 fs-2"></i>
                            </span>        
                            <span class="menu-title">Dashboard</span>
                        </a>               
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*users*') ? 'active' : ''}}" href="{{ route('users.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-abstract-41 fs-2"></i>
                            </span>    
                            <span class="menu-title">User Management</span>
                        </a>                
                    </div>
                </div>
            </div>
        </div>
    </div>