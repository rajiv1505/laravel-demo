@include('layouts.partials.header')
@include('layouts.partials.sidebar')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
	<div class="d-flex flex-column flex-column-fluid">
		<div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10">
			<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
				<div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
					<div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
						<h1 class="page-heading d-flex flex-column justify-content-center text-dark fw-bold fs-3 m-0">Panel Configuration</h1>
						<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
							<li class="breadcrumb-item text-muted">
								<a href="{{ route('home') }}" class="text-muted text-hover-primary">Home</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">Panel Configuration</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="kt_app_content" class="app-content flex-column-fluid">
			<div id="kt_app_content_container" class="app-container container-fluid">
				@if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{!! \Session::get('success_message') !!}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (\Session::has('error'))
				    <div class="alert alert-danger alert-dismissible fade show" role="alert">
				      <strong>{!! \Session::get('error') !!}</strong>
				      <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
				    </div>
				@endif
				<div class="card mb-5 mb-xl-10">
					<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
						<div class="card-title m-0">
							<h3 class="fw-bold m-0">Configuration</h3>
						</div>
					</div>
					<div id="kt_account_settings_profile_details" class="collapse show">
						<form action="{{ route('config.update',$config->id) }}" method="POST" class="form" enctype="multipart/form-data">
							@csrf
                  			@method('PUT')
							<div class="card-body border-top p-9">
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Logo</label>
									<div class="col-lg-8">
										<div class="image-input image-input-outline" data-kt-image-input="true">
											<div class="image-input-wrapper w-125px h-125px" style="background-image: {{ isset($config) && $config->logo ? 'url('.asset('Config/Logo/'. $config->logo).')' : 'none' }};"></div>
											<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change logo">
												<i class="ki-outline ki-pencil fs-7"></i>
												<input type="file" name="logo" accept=".png, .jpg, .jpeg" />
												<input type="hidden" name="custom_delete_logo" />
											</label>
										</div>
										<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Favicon</label>
									<div class="col-lg-8">
										<div class="image-input image-input-outline" data-kt-image-input="true">
											<div class="image-input-wrapper w-125px h-125px" style="background-image: {{ isset($config) && $config->favicon ? 'url('.asset('Config/Favicon/'. $config->favicon).')' : 'none' }};"></div>
											<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change favicon">
												<i class="ki-outline ki-pencil fs-7"></i>
												<input type="file" name="favicon" accept=".png, .jpg, .jpeg" />
												<input type="hidden" name="custom_delete_favicon" />
											</label>
										</div>
										<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Title & Email</label>
									<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-6 fv-row">
												<input type="text" name="title" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0 @error('name') is-invalid @enderror" placeholder="Title" value="{{ old('title', optional($config)->title) }}" />
												@error('name')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
											<div class="col-lg-6 fv-row">
												<input type="email" name="email" class="form-control form-control-lg form-control-solid @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', optional($config)->email) }}" />
												@error('name')
		                                        <span class="invalid-feedback" role="alert">
		                                            <strong>{{ $message }}</strong>
		                                        </span>
		                                    	@enderror
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Mobile No</label>
									<div class="col-lg-8 fv-row">
										<input type="number" name="mobile_no" class="form-control form-control-lg form-control-solid" placeholder="Mobile no" value="{{ old('mobile_no', optional($config)->mobile_no) }}" />
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label fw-semibold fs-6">Address</label>
									<div class="col-lg-8 fv-row">
										
										<textarea name="address" class="form-control form-control-lg form-control-solid">{{ old('address', optional($config)->address) }}</textarea>
									</div>
								</div>
								<div class="row mb-6">
									<label class="col-lg-4 col-form-label required fw-semibold fs-6">Footer</label>
									<div class="col-lg-8 fv-row">
										<input type="text" name="footer" class="form-control form-control-lg form-control-solid" placeholder="Name of extra" value="{{ old('footer', optional($config)->footer) }}" />
									</div>
								</div>
							</div>
							<div class="card-footer d-flex justify-content-end py-6 px-9">
								<button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2">Discard</button>
								<button type="submit" value="Update" class="btn btn-sm btn-primary">Save Changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.partials.footer')

<script src="{{ asset('admin/dist/assets/js/custom/account/settings/signin-methods.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/account/settings/profile-details.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/account/settings/deactivate-account.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/pages/user-profile/general.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-campaign.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/type.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/details.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/finance.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/complete.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/main.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/two-factor-authentication.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>
