<!DOCTYPE html>

<html lang="en">
    <head><base href="../../../"/>
        @if(isset($config->title))
        <title>{{$config->title}}</title>
        @else
        <title>Laravel-Demo</title>
        @endif
        <meta charset="utf-8" />
        <meta name="description" content="Laravel-Demo" />
        <meta name="keywords" content="Laravel-Demo" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Laravel-Demo" />
        <meta property="og:url" content="Laravel-Demo" />
        <meta property="og:site_name" content="Laravel-Demo" />
        <link rel="canonical" href="#" />
        @if(isset($config->title))
        <link rel="shortcut icon" href="{{asset('Config/Favicon/').'/'.$config->favicon}}" />
        @else
        <link rel="shortcut icon" href="#" />
        @endif
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
        <!--end::Fonts-->
        <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
        <link href="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.css') }}"  rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/dist/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Global Stylesheets Bundle-->
    </head>
    <!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="app-blank">
        <!--begin::Theme mode setup on page load-->
        <script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if ( localStorage.getItem("data-bs-theme") !== null ) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>
        <!--end::Theme mode setup on page load-->
        <!--begin::Root-->
        <div class="d-flex flex-column flex-root" id="kt_app_root">
            <!--begin::Authentication - Sign-in -->
            <div class="d-flex flex-column flex-lg-row flex-column-fluid">
                <!--begin::Body-->
                <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                    <!--begin::Form-->
                    <div class="d-flex flex-center flex-column flex-lg-row-fluid">
                        <!--begin::Wrapper-->
                        <div class="w-lg-500px p-10">
                            <!--begin::Form-->
                            <form method="POST" class="form w-100" action="{{ route('login') }}">
                                @csrf
                                <div class="text-center mb-11">
                                    <h1 class="text-dark fw-bolder mb-3">Sign In</h1>
                                </div>
                                <div class="fv-row mb-8">
                                    <input type="email" placeholder="Email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror bg-transparent" required autocomplete="email" autofocus />

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="fv-row mb-3">
                                    <input type="password" placeholder="Password" name="password" autocomplete="off" class="form-control @error('password') is-invalid @enderror bg-transparent" required autocomplete="current-password" />

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-8">
                                    <div></div>
                                    @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}" class="link-primary">Forgot Password ?</a>
                                    @endif
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Submit button-->
                                <div class="d-grid mb-10">
                                    <button type="submit" class="btn btn-primary">
                                        <!--begin::Indicator label-->
                                        <span class="indicator-label">Sign In</span>
                                        <!--end::Indicator label-->
                                        <!--begin::Indicator progress-->
                                        <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                                <!--end::Submit button-->
                                <!--begin::Sign up-->
                                <div class="text-gray-500 text-center fw-semibold fs-6">Not a Member yet?
                                @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="link-primary">Sign up</a></div>
                                @endif
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                    <div class="w-lg-500px d-flex flex-stack px-10 mx-auto">
                        <div class="d-flex fw-semibold text-primary fs-base gap-5">

                        </div>
                    </div>
                </div>
                <!--end::Body-->
                <!--begin::Aside-->
                <div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-color: #c3ffd2;">
                    
                    <div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
                        <a href="#" class="mb-0 mb-lg-12">
                            <img alt="Logo" src="{{ asset('admin/dist/assets/media/misc/laravel.png') }}" class="h-60px h-lg-75px" /> 
                        </a>
                        <!--end::Logo-->
                        <!--begin::Image-->
                        <!-- <img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-500px mb-10 mb-lg-20" src="{{ asset('admin/dist/assets/media/misc/image 37.png') }}" alt="" /> -->
                        <!--end::Image-->
                        <!--begin::Title-->
                        <h1 class="d-none d-lg-block text-dark fs-2qx fw-bolder text-center mb-7">Laravel Demo</h1>
                        <!--end::Text-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Aside-->
            </div>
            <!--end::Authentication - Sign-in-->
        </div>
        <!--end::Root-->
        <!--begin::Javascript-->
        <script>var hostUrl = "assets/";</script>
        <!--begin::Global Javascript Bundle(mandatory for all pages)-->
        <script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ asset('admin/dist/assets/js/scripts.bundle.js') }}"></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Custom Javascript(used for this page only)-->
        <script src="{{ asset('admin/dist/assets/js/custom/authentication/sign-in/general.js') }}"></script>
        <!--end::Custom Javascript-->
        <!--end::Javascript-->
    </body>
    <!--end::Body-->
</html>