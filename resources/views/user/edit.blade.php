@include('layouts.partials.header')
@include('layouts.partials.sidebar')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
	<div class="d-flex flex-column flex-column-fluid">
		<div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10">
			<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
				<div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
					<div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
						<h1 class="page-heading d-flex flex-column justify-content-center text-dark fw-bold fs-3 m-0">User Edit</h1>
						<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
							<li class="breadcrumb-item text-muted">
								<a href="{{ route('home') }}" class="text-muted text-hover-primary">Home</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">
								<a href="{{ route('users.index') }}" class="text-muted text-hover-primary">List</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">Edit</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="kt_app_content" class="app-content flex-column-fluid">
			<div id="kt_app_content_container" class="app-container container-fluid">
				@if (\Session::has('error'))
				    <div class="alert alert-danger alert-dismissible fade show" role="alert">
				      <strong>{!! \Session::get('error') !!}</strong>
				      <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
				    </div>
				@endif
				<form method="POST" action="{{ route('users.update',$user->id) }}" accept-charset="UTF-8" id="create_user_form" name="create_user_form" class="form d-flex flex-column flex-lg-row" enctype="multipart/form-data">
					{{ csrf_field() }}
					@method('PUT')
					<div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 me-lg-10 mb-7">
						<div class="d-flex flex-column gap-7 gap-lg-10">
							<div class="card card-flush py-4">
								<div class="card-header">
									<div class="card-title">
										<h2>User Details</h2>
									</div>
								</div>
								<div class="card-body pt-0">
									<div class="row gx-10 mb-5">
										<div class="col-lg-6">
											<label class="required form-label">Name</label>
											<input type="text" name="name" class="form-control mb-2 @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name', optional($user)->name) }}" />

											@error('name')
	                                        <span class="invalid-feedback" role="alert">
	                                            <strong>{{ $message }}</strong>
	                                        </span>
	                                    	@enderror
										</div>
										<div class="col-lg-6">
											<label class="required form-label">Email</label>
											<input type="email" name="email" class="form-control mb-2 @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', optional($user)->email) }}" />

											@error('email')
	                                        <span class="invalid-feedback" role="alert">
	                                            <strong>{{ $message }}</strong>
	                                        </span>
	                                    	@enderror
										</div>
									</div>
									<div class="row gx-10 mb-5">
										<div class="col-lg-6">
											<label class="form-label">Street Address</label>
											<input type="text" name="street_address" class="form-control mb-2 @error('street_address') is-invalid @enderror" placeholder="Street address" value="{{ old('street_address', optional($user)->street_address) }}" />
										</div>
										<div class="col-lg-6">
											<label class="form-label">City</label>
											<input type="text" name="city" class="form-control mb-2 @error('city') is-invalid @enderror" placeholder="City" value="{{ old('city', optional($user)->city) }}" />
										</div>
									</div>
									<div class="row gx-10 mb-5">
										<div class="col-lg-6">
											<label class="form-label">Taluka</label>
											<input type="text" name="taluka" class="form-control mb-2 @error('taluka') is-invalid @enderror" placeholder="Taluka" value="{{ old('taluka', optional($user)->taluka) }}" />
										</div>
										<div class="col-lg-6">
											<label class="form-label">District</label>
											<input type="text" name="district" class="form-control mb-2 @error('district') is-invalid @enderror" placeholder="District" value="{{ old('district', optional($user)->district) }}" />
										</div>
									</div>
									<div class="row gx-10 mb-5">
										<div class="col-lg-6">
											<label class="form-label">State</label>
											<input type="text" name="state" class="form-control mb-2 @error('state') is-invalid @enderror" placeholder="State" value="{{ old('state', optional($user)->state) }}" />
										</div>
										<div class="col-lg-6">
											<label class="form-label">Pin No</label>
											<input type="number" name="pin_code" class="form-control mb-2 @error('pin_code') is-invalid @enderror" placeholder="Pin code" value="{{ old('pin_code', optional($user)->pin_code) }}" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="d-flex justify-content-end">
							<a href="{{ route('users.index') }}" class="btn btn-sm btn-light me-5">Cancel</a>
							<button type="submit" value="Add" class="btn btn-sm btn-primary">
								<span class="indicator-label">Save Changes</span>
							</button>
						</div>
					</div>
					<div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px">
						<div class="card card-flush py-4">
							<div class="card-header">
								<div class="card-title">
									<h5>Profile Photo</h5>
								</div>
							</div>
							<div class="card-body text-center pt-0">
							    <style>
							        .image-input-placeholder {
							            background-image: url('{{ isset($user) && $user->photo ? asset('Profile/photos/'. $user->photo) : asset('admin/dist/assets/media/svg/files/blank-image.svg') }}');
							        }

							        [data-bs-theme="dark"] .image-input-placeholder {
							            background-image: url('{{ isset($user) && $user->photo ? asset('Profile/photos/'. $user->photo) : asset('admin/dist/assets/media/svg/files/blank-image.svg') }}');
							        }
							    </style>
							    <div class="image-input image-input-empty image-input-outline image-input-placeholder mb-3" data-kt-image-input="true">
							        <div class="image-input-wrapper w-150px h-150px"></div>
							        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change photo">
							            <i class="ki-outline ki-pencil fs-7"></i>
							            <input type="file" name="photo" accept=".png, .jpg, .jpeg" />
							            <input type="hidden" name="custom_delete_photo" />
							        </label>
							        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel photo">
							            <i class="ki-outline ki-cross fs-2"></i>
							        </span>
							        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove photo">
							            <i class="ki-outline ki-cross fs-2"></i>
							        </span>
							    </div>
							</div>
						</div>
						<div class="card card-flush py-4">
							<div class="card-header">
								<div class="required card-title">
									<h5>Status</h5>
								</div>
							</div>
							<div class="card-body pt-0">
								<select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select @error('status') is-invalid @enderror">
	                                <option value="" style="display: none;" {{ old('status', optional($user)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
	                                @foreach (['active' => 'Active','inactive' => 'Inactive'] as $key => $text)
	                                    <option value="{{ $key }}" {{ old('status', optional($user)->status) == $key ? 'selected' : '' }}>
	                                        {{ $text }}
	                                    </option>
	                                @endforeach
                            	</select>
                            	@error('status')
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $message }}</strong>
	                                </span>
                            	@enderror
							</div>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
@include('layouts.partials.footer')