@include('layouts.partials.header')
@include('layouts.partials.sidebar')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
		<div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10">
			<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
				<div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
					<div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
						<h1 class="page-heading d-flex flex-column justify-content-center text-dark fw-bold fs-3 m-0">User List</h1>
						<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
							<li class="breadcrumb-item text-muted">
								<a href="{{ route('home') }}" class="text-muted text-hover-primary">Home</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">User Management</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">Users</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="kt_app_content" class="app-content flex-column-fluid">
			<div id="kt_app_content_container" class="app-container container-fluid">
				@if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{!! \Session::get('success_message') !!}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (\Session::has('error'))
				    <div class="alert alert-danger alert-dismissible fade show" role="alert">
				      <strong>{!! \Session::get('error') !!}</strong>
				      <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
				    </div>
				@endif
				<div class="card">
					<div class="card-header border-0 pt-6">
						<div class="card-title">
							<div class="d-flex align-items-center position-relative my-1">
								<i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
								<input type="text" data-kt-user-table-filter="search" class="form-control form-control-solid w-250px ps-13" placeholder="Search User" />
							</div>
						</div>
						<div class="card-toolbar">
							<div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
								<a type="button" class="btn btn-sm btn-primary me-3" href="{{ route('users.create')}}">
								<i class="ki-outline ki-plus fs-2"></i>Add User</a>
							</div>
							<div class="d-flex justify-content-end align-items-center d-none" data-kt-user-table-toolbar="selected">
								<div class="fw-bold me-5">
								<span class="me-2" data-kt-user-table-select="selected_count"></span>Selected</div>
								<button type="button" class="btn btn-danger" data-kt-user-table-select="delete_selected">Delete Selected</button>
							</div>
						</div>
					</div>
					<div class="card-body py-4">
						<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
							<thead>
								<tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
									<th class="min-w-125px">Sr No</th>
									<th class="min-w-125px">Name</th>
									<th class="min-w-125px"></th>
									<th class="min-w-125px">City</th>
									<th class="min-w-125px">Taluka</th>
									<th class="min-w-125px">Status</th>
									<th class="min-w-125px">Actions</th>
								</tr>
							</thead>
							<tbody class="text-gray-600 fw-semibold">
								<?php $i = 1; ?>
								@foreach($users as $user)
								<tr>
									<td>{{ $i++ }}</td>
									<td class="d-flex align-items-center">
										<!--begin:: Avatar -->
										<div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                            @if(!is_null($user->photo))
											<span>
												<div class="symbol-label">
													<img src="{{ asset('Profile/photos/' . $user->photo) }}" alt="Emma Smith" class="w-100" />
												</div>
											</span>
											@else
											<span>
												<div class="symbol-label">
													<img src="{{ asset('admin/dist/assets/media/svg/avatars/blank.svg') }}" alt="Emma Smith" class="w-100" />
												</div>
											</span>
											@endif
										</div>
										<div class="d-flex flex-column">
											<a href="{{ route('users.edit',$user->id) }}" class="text-gray-800 text-hover-primary mb-1">{{ $user->name }}</a>
											<span>{{ $user->email }}</span>
										</div>
									</td>
									<td></td>
									<td>{{ $user->city }}</td>
									<td>{{ $user->taluka }}</td>
									<td>@if($user->status == 'active')
										<div class="badge badge-light-success fw-bold">Active</div>
										@elseif($user->status == 'inactive')
										<div class="badge badge-light-danger fw-bold">Inactive</div>
										@endif
									</td>
									<td>
										<a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
										<i class="ki-outline ki-down fs-5 ms-1"></i></a>
										<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
											@if($user->email === Auth::user()->email )
											<div class="menu-item px-3">
												<a href="{{ route('users.edit',$user->id) }}" class="menu-link px-3">Edit</a>
											</div>
											@else
											<div class="menu-item px-3">
                                                    <a href="{{ route('users.edit',$user->id) }}" class="menu-link px-3">Edit</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <form method="POST" action="{{ route('users.destroy', $user['id']) }}" accept-charset="UTF-8">
                                                        <input name="_method" value="DELETE" type="hidden">
                                                        {{ csrf_field() }}
                                                        <button type="submit" title="delete" class="btn btn-sm w-100 menu-link px-3" data-id="#" onclick="return confirm(&quot;Click Ok to delete User.&quot;)" >
                                                            Delete
                                                        </button>
                                                    </form>
                                                </div>
                                                @endif
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.partials.footer')

<script>var hostUrl = "assets/";</script>
<script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/scripts.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Vendors Javascript(used for this page only)-->
<script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<!--end::Vendors Javascript-->
<!--begin::Custom Javascript(used for this page only)-->
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/table.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/export-users.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/add.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-campaign.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>