<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();

        return view('profile.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->name = $request->input('name');
        $user->email = $request->input('email'); 
        $user->mandal_registration_number = $request->input('mandal_registration_number');
        $user->mandal_sanchalak_name = $request->input('mandal_sanchalak_name');
        $user->mandal_sanchalak_phone_no = $request->input('mandal_sanchalak_phone_no');
        $user->mandal_nirixak_name = $request->input('mandal_nirixak_name');
        $user->mandal_nirixak_mobile = $request->input('mandal_nirixak_mobile');
        $user->name_of_extra = $request->input('name_of_extra');
        $user->street_address = $request->input('street_address');
        $user->city = $request->input('city');
        $user->taluka = $request->input('taluka');
        $user->district = $request->input('district');
        $user->pin_code = $request->input('pin_code');
        $user->state = $request->input('state');
        $user->type_of_mandal = $request->input('type_of_mandal');

        if ($request->has('custom_delete_photo')) {

            if ($user->photo) {
                $photoPath = public_path('Profile/photos') . '/' . $user->photo;
                if (file_exists($photoPath)) {
                    unlink($photoPath);
                }
                $user->photo = null;
            }
        } 

        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $fileName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('Profile/photos'), $fileName);

            if ($user->photo) {
                $photoPath = public_path('Profile/photos/') . $user->photo;
                if (file_exists($photoPath)) {
                    unlink($photoPath);
                }
            }

            $user->photo = $fileName;
        }


      //  dd($user->photo);
        $user->update();

        return redirect()->route('profile.index')
            ->with('success_message', 'Profile updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function change(Request $request, $id)
    {
        $request->validate([
            'password' => 'required',
            'confirm-password' => 'required',
        ]);


        $userId = Auth::user()->id;
        $user = User::find($userId);
        $psw = $request->input('password');
        $cnfpsw = $request->input('confirm-password');
        $user->password = Hash::make($psw);

        if ($psw === $cnfpsw) {
            $user->update();

            return redirect()->route('profile.index')
                ->with('success_message', 'Password changed successfully');
        } else {
            return redirect()->route('profile.index')
                ->with('error', 'Password and Confirm Password must be same !');
        }

    }
}
