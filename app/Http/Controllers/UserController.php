<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = null;
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email'=> 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            
        ]);


        $photo = $request->file('photo');
        $fileName = time() . '.' . $photo->getClientOriginalExtension();
        $photo->move(public_path('Profile/photos'), $fileName);

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->street_address = $request->input('street_address');
        $user->city = $request->input('city');
        $user->taluka = $request->input('taluka');
        $user->district = $request->input('district');
        $user->pin_code = $request->input('pin_code');
        $user->state = $request->input('state');
        $user->status = $request->input('status', 'active');
        $password = $request->input('password');
        $user->password = Hash::make($password);
        $cnfpsw = $request->input('password_confirmation');
        $user->photo = $fileName;
        
       // dd($user);

        if ($password === $cnfpsw) {

            $user->save();

            return redirect()->route('users.index')
                ->with('success_message', 'Mandal successfully Create');
        } else {
            return redirect()->back()
                ->with('error', 'Password and Confirm Password must be same !');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        return view('user.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'email'=> 'required',
            'status'=> 'required',
            
        ]);

        $user = User::find($id);

        if ($request->hasFile('photo')) {

            $photo = $request->file('photo');
            $fileName = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move(public_path('Profile/photos'), $fileName);

            if ($user->photo) {
                $oldPhotoPath = public_path('Profile/photos/') . $user->photo;
                if (file_exists($oldPhotoPath)) {
                    unlink($oldPhotoPath);
                }
            }
            $user->photo = $fileName;
        }

        
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->street_address = $request->input('street_address');
        $user->city = $request->input('city');
        $user->taluka = $request->input('taluka');
        $user->district = $request->input('district');
        $user->pin_code = $request->input('pin_code');
        $user->state = $request->input('state');
        $user->status = $request->input('status');
        $user->update();
        return redirect()->route('users.index')
            ->with('success_message', 'Mandal updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('users.index') 
                ->with('success_message',' Mandal successfully deleted');
    }
}
