<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Config;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $config = Config::first();
        return view('config.index', compact('config'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $config = Config::find($id);
        
        
        $config->title = $request->input('title');
        $config->mobile_no = $request->input('mobile_no');
        $config->email = $request->input('email');
        $config->address = $request->input('address');
        $config->footer = $request->input('footer');

        if($image = $request->file('logo')) {
            $path = 'Config/Logo/';
            $uniq = rand();
            $logo = date('YmdHis'). $uniq. "." .$image->getClientOriginalExtension();
            $image->move($path, $logo);
            $config->logo = "$logo";
        }

        if($image = $request->file('favicon')) {
            $path = 'Config/Favicon/';
            $uniq = rand();
            $favicon = date('YmdHis') . $uniq. "." .$image->getClientOriginalExtension();
            $image->move($path, $favicon);
            $config->favicon = "$favicon";
        }


        $config->update();

        return redirect()->route('config.index')->with('success_message', 'Your data has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
