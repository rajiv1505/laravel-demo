<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ConfigController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
        return view('welcome');
    });

Auth::routes();

Route::group(['middleware' => ['auth']], function() {

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Config

Route::resource('config', ConfigController::class);

// User

Route::resource('users', UserController::class);

// Profile

    Route::resource('profile', ProfileController::class);

    Route::post('/profile/{id}', [App\Http\Controllers\ProfileController::class, 'change'])->name('profile.change');

});


